const express = require('express');
const path = require('path')
const app = express();
const port = 3000;

app.use(express.static('public'));

app.use('/styles', express.static(__dirname + '/public/styles.css'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/home.html');
});

app.get('/desenvolvimento', (req, res) => {
  res.sendFile(__dirname + '/views/desenvolvimento.html');
});

app.get('/objetivos', (req, res) => {
  res.sendFile(__dirname + '/views/objetivos.html');
});

app.get('/conclusao', (req, res) => {
  res.sendFile(__dirname + '/views/conclusao.html');
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
